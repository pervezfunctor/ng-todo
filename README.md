###Install Node

####Mac

Install homebrew

    ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"
    brew doctor

Install node using brew

    brew install node

####Linux(Ubuntu)

    sudo add-apt-repository ppa:chris-lea/node.js
    sudo apt-get update && sudo apt-get upgrade -y
    sudo apt-get install nodejs


###Install bower, grunt and karma

    npm install -g bower grunt-cli karma

###Install git

####Mac

    brew install git

####Linux

    sudo apt-get install git

###Setup Web Project

Clone this repository
    git clone https://pervezfunctor@bitbucket.org/pervezfunctor/seartipy-ng-seed.git

Install bower and npm dependencies

    cd seartipy-ng-seed
    bower install
    npm install

Start the server

    grunt

Open url *https://localhost:8000/index.html*
