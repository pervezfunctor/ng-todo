// Please run 'npm install' and 'bower install' in this directoy before using grunt
module.exports = function(grunt) {

    require('load-grunt-tasks')(grunt); // no need for loadNpmTask each grunt plugin

    grunt.registerTask('serve', ['connect:server']);
    grunt.registerTask('default', ['jade', 'concurrent:default']);

    return grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        jade: {
            compile: {
                expand: true,
                cwd: 'src/jade',
                src: ['**/*.jade'],
                dest: 'build/html/',
                ext: '.html'
            }
        },

        connect: {
            server: {
                options: {
                    port: 8000,
                    keepalive: true,
                    livereload: true,
                    base: ['build/html', 'src/js', 'bower_components']
                }
            }
        },

        watch: {
            jade: {
                files: ['src/jade/**/*.jade'],
                tasks: ['jade']
            },
            js: {
                files: ['src/js/**/*.js'],
                tasks: []
            },
            options: {
                spawn: true,
                // reloads browser when any js or jade file changes
                livereload: true
            }
        },

        concurrent: { // allows us to start static file server as well as watch files
            default: ['serve', 'watch'],
            options: {
                logConcurrentOutput: true
            }
        }
    });
};
