var todoApp = angular.module('todoApp', []);

todoApp.value('todoItems', [
    { id: 1, title: 'read SQL', completed: false },
    { id: 2, title: 'learn entity framework query optimization', completed : false },
    { id : 3, title : 'learn ui-router, angular-bootstrap, restangular', completed : false }
]);

todoApp.controller('TodoCtrl', function($scope, todoItems) {
    $scope.todoItems = todoItems;
});
